package application;

import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


/**
 * Classe permettant de faire le lien et initialiser les variables utilis�es dans le Scene Builder GUI_CustomShoppingList.fxml
 */
public class ControllerCustom implements Initializable {
    
	// Variables utilisées sur le Scene Builder 
    @FXML private ListView<String> shoppingList;
    @FXML private TextArea countTest;
    @FXML private Button lessBut;
    @FXML private Button plusBut;
    @FXML private ListView<String> products;
    @FXML private TextArea newProductName;
    @FXML private Button saveButton;
    @FXML private TextField quantityVal;
    
    // Map contenant la liste des ingr�dients et leur quantité
	private Map<String, Integer> ingredients = new LinkedHashMap<>();
	
	private String[] productsList = {"Orange","Fraise","Poire","Lait","Oeuf","P�tes","Jus de fruit","Tomate","C�r�ales","Muffin","Biscuit"};
    
	/**
	 * Permet d'augmenter la quantit� d'un produit lorsqu'on clique sur le bouton + 
	 */
    public void plusButPushed(){
   	 	String selected = shoppingList.getSelectionModel().getSelectedItem();
   	 	if(selected != null && !selected.isEmpty()){
	   	 	ingredients.put(selected, ingredients.get(selected) + 1); 
	        String count = String.valueOf(ingredients.get(selected)); 
	        countTest.setText(count);
   	 	}
    }
    
    /**
	 * Permet d'augmenter la quantit� d'un produit lorsqu'on clique sur le bouton -
	 */
    public void lessButPushed(){
    	String selected = shoppingList.getSelectionModel().getSelectedItem();
    	if(selected != null && !selected.isEmpty()){
	    	if(ingredients.get(selected) > 0){
	    		ingredients.put(selected, ingredients.get(selected) - 1); 
	            String count = String.valueOf(ingredients.get(selected)); 
	            countTest.setText(count);
	    	}
    	}
    }
    
    /**
 	 * Permet de r�cup�rer la quantit� d'un ingr�dient s�lectionn� dans la liste de courses
 	 */
    public void getValueOfIngredient(){
    	 String selected = shoppingList.getSelectionModel().getSelectedItem();
         Integer ingValue = ingredients.get(selected);
         String ingValueString = String.valueOf(ingValue);
         countTest.setText(ingValueString);
    }
    
    
    /**
     * Permet de changer manuellement la quantit� d'un produit s�lectionn�
     */
    public void changeProductVal(String value){
    	String selected = shoppingList.getSelectionModel().getSelectedItem();
    	String typedVal = value;
    	if(isNumeric(typedVal)){
    		int typedValInt = Integer.parseInt(typedVal);
	    	if(typedValInt > 0){
	    		ingredients.put(selected, typedValInt); 
	            String count = String.valueOf(ingredients.get(selected)); 
	            countTest.setText(count);
	    	}
    	}
    }
    
    
    /**
     * Permet d'ajouter un produit � la liste d'ingr�dients
     */
    public void addProduct(String product){
    	if(ingredients.get(product) == null){
	    	ingredients.put(product, 1);
	    	shoppingList.getItems().add(product);
    	}
    	else{
    		ingredients.put(product, ingredients.get(product) + 1);
    		countTest.setText(String.valueOf(ingredients.get(product))); // r�gler le souci de s�lection 
    	}
    }
    
    /**
     * Permet d'ajouter un produit disponible dans la base de donn�es � la liste d'ingr�dients
     */
    public void addProductToList(){
    	String selected = products.getSelectionModel().getSelectedItem();
    	addProduct(selected);
    }
    
    /**
     * Permet d'ajouter un produit non disponible dans la base de donn�es � la liste d'ingr�dients
     */
    public void addNewProductToList(){
    	String prodName = newProductName.getText();
    	addProduct(prodName);
    }
    
    /**
     * Supprime un �l�ment de la liste d'ingr�dients
     */
    public void deleteElemFromList(){
    	String selected = shoppingList.getSelectionModel().getSelectedItem();
    	ingredients.remove(selected);
    	shoppingList.getItems().remove(selected);
    }
    
    /* m�thode qui contiendra la fonction du bouton sauvegarder
     * 
    public void saveInDatabase(){
    	
    } */
    
    /*
     * M�thode quand on appuie sur back
     
    public void backButtonPushed(){
    	
    } */
    
    /*
     * M�thode quand on appuie sur deco
     
    public void dcButtonPushed(){
    	
    } */
    
    /**
 	 * V�rifie si un string est bien un nombre
 	 * 
 	 * @param str : string à vérifier
 	 * @return True si c'est bien un nombre, False sinon 
 	 */
    public static boolean isNumeric(String str) {
      try {
        double d = Double.parseDouble(str);
      }
      catch(NumberFormatException nfe) {
        return false;
      }
      return true;
    }
    
    /**
 	 * Initialise l'interface graphique (les variables du scene builder)
 	 * 
 	 * @param url , rb : injection de propriétés de localisation et de ressources dans le contrôleur.
 	 */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	
        ingredients.put("Banane", 2);
        ingredients.put("Pomme", 3);
        ingredients.put("Chocolat", 1);
        
        shoppingList.getItems().addAll(ingredients.keySet());
        shoppingList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        products.getItems().addAll(productsList);
        products.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        
        // Update value of ingredient with typed value 
    	countTest.textProperty().addListener((observable, oldValue, newValue) -> {
    		changeProductVal(newValue);
    	});
       
        

    }    
    
}