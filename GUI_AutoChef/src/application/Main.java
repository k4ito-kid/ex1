package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Classe principale ex�cutant l'interface graphique
 */
public class Main extends Application{
	
	public static void main(String[] args){
		launch(args);
	}	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root  = FXMLLoader.load(getClass().getResource("GUI_CustomShoppingList.fxml")); // Permet de r�cup�rer les donn�es du Scene Builder
		Scene scene = new Scene(root);
		primaryStage.setTitle("App");
		primaryStage.setScene(scene);
		primaryStage.show();

	}
}