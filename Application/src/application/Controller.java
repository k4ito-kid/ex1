package application;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;

public class Controller {
	@FXML
    private ChoiceBox<String> prod ;
	
	public void handle()
	{
		prod.getItems().clear();
		String[] produ = {"Cheese", "Pepperoni", "Black Olives"};
        for(int i=0;i<produ.length;i++ ){
        	
        	prod.getItems().add(produ[i]);
        	
        }

	}
	
}
